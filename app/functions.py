__author__ = 'Kunal Monga'
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT
import json, os
from app.model import model
from flask_mail import Message
from bson.objectid import ObjectId



def sendEmail(sender,recipient,subject,content,attachment):
	msg = Message(subject, sender=sender, recipients=[recipient])
	if attachment !='':
		with open(os.path.join(APP_ROOT	, attachment)) as fp:
			pass
		# msg.attach('app/'+attachment, "application/pdf", fp.read())
	msg.html = content
	mail.send(msg)
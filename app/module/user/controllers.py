__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, functions, APP_STATIC
import json, urllib, datetime, email, re,uuid, hashlib, urllib, os, random, string
from bs4 import BeautifulSoup
from app.model import model
from app.model.model import *
from sqlalchemy import desc
# Token, Users, UserAddress, Doctor, DoctorAddress, DoctorSpeciality, Blog, Comment
from bson.objectid import ObjectId
from app.module.validation import validate, msgtext

# Define the blueprint: 'auth', set its url prefix: app.url/auth
user_link = Blueprint('user', __name__, url_prefix='/user')

# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/login/', methods=['POST'])
def login():
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"login",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #	
	try:
		if validData==True:
			password = hashlib.md5(str(data['password']).encode())
			getUser = Users.query.filter_by(email=str(data["user_name"]),password = password.hexdigest()).first()
			if getUser:
				responseData["user_id"] = str(getUser.id)
				responseData["user_name"] = getUser.user_name
				responseData["first_name"] = getUser.first_name
				responseData["email"] = getUser.email
				content["status"] = 1
				content["message"] = msgtext.success
			else:
				getUserByPhone = Users.query.filter_by(phone=str(data["user_name"]), password = password.hexdigest()).first()
				if getUserByPhone:
					responseData["user_id"] = str(getUserByPhone.id)
					responseData["user_name"] = getUserByPhone.user_name
					responseData["first_name"] = getUserByPhone.first_name
					responseData["email"] = getUserByPhone.email
					content["status"] = 1
					content["message"] = msgtext.success
				else:
					content["status"] = 0
					content["message"] = "User/Password Does not matched"
	except Exception as e:
		print e
	content["data"] = responseData
	content["message"] = content["message"].replace("_"," ")
	return jsonify(content)

# Login In  API Ends
# Return json with the status of login

# Register In  API starts
# Takes username detail and register 
# Set the route and accepted methods
@user_link.route('/register/', methods=['POST'])
def register():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	data = request.json
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"register",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #
	try:
		if validData==True:
			checkuser = Users.query.filter_by(email=str(data["email"])).first()
			if checkuser:
				content = {
			               'status' : 0,	
			               'message' : 'User Already Exit.'
			               }
			else:
				password = hashlib.md5(str(data["password"]).encode())
				userObj = Users(str(data["email"]),data["first_name"],data["last_name"],str("Username"),password.hexdigest(),str(data["phone"]))
				db.session.add(userObj)
	    		db.session.commit()
	    		responseData["user_id"] = str(userObj.id)
	    		responseData["user_name"] = userObj.user_name
	    		responseData["first_name"] = userObj.first_name
	    		responseData["email"] = userObj.email
	    		rand = ''
	    		rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(9)])
	    		tokenObj= Token(str(rand),userObj.id)
	    		db.session.add(tokenObj)
	    		db.session.commit()

	    		sock = urllib.urlopen(os.path.join(APP_STATIC, 'register.html'))
	    		htmlSource = sock.read()
	    		sock.close()
	    		subject = "Welcome to Healora."
	    		htmlSource = string.replace(htmlSource, "[token]", str(rand))
	    		res = functions.sendEmail("help@healora.com",userObj.email,subject,htmlSource,"")
	    		content = {
				'status' : 1,	
				'message' : 'Success',
				'user_id' : str(userObj.id)
				}

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content['data'] = responseData
	return jsonify(content)
# Register In  API Ends
# Return json with the status of login


@user_link.route('/edata/', methods=['POST'])
def edata():
	data = request.json
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	
	# Custom Validation starts #
	u = Users.query.filter_by(id=data["_id"]).first()
	A = UserAddress.query.filter_by(user_id=data["_id"]).first()
	# if checkuser:
	# 	content = {
	# 				"v":1
	# 				}
	try:
		if u:
			print "here"
			#password = hashlib.md5(str(data["password"]).encode())	
			responseData["email"] = str(u.email)
			#"password":password.hexdigest(),
			responseData["first_name"] = str(u.first_name)
			responseData["last_name"] = str(u.last_name)
			responseData["phone"] = str(u.phone)
							
			content = {
						'status' : 1,
						'message' : 'Success'
						}
		else:
			print "else"
			content = {
						'status' : 0,
						'message' : 'Invalid data'
			        	}

		if A:
			print "address"
			responseData["address_line1"] = str(A.address_line1)
			responseData["address_line2"] = str(A.address_line2)
			responseData["appartment"] = str(A.appartment)
			responseData["city"] = str(A.city)
			responseData["state"] = str(A.state)
			responseData["loc_lat"] = str(A.loc_lat)
			responseData["loc_lon"] = str(A.loc_lon)
			responseData["zipcode"] = str(A.zipcode)
			responseData["company"] = str(A.company)
							
			content = {
						'status' : 1,
						'message' : 'Success'
						}
		else:
			responseData["address_line1"] = ""
			responseData["address_line2"] = ""
			responseData["appartment"] = ""
			responseData["city"] = ""
			responseData["state"] = ""
			responseData["loc_lat"] = ""
			responseData["loc_lon"] = ""
			responseData["zipcode"] = ""
			responseData["company"] = ""

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content["data"] = responseData
	return jsonify(content)


@user_link.route('/edit/', methods=['POST'])
def edit():
	
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }

	data = request.json
	
		
	# Custom Validation starts #
	checkuser = Users.query.filter_by(id=data["_id"]).first()
	Address = UserAddress.query.filter_by(user_id=data["_id"]).first()
	
	try:
		if checkuser:
			print "here"
			checkuser.email = data["email"]
			checkuser.first_name  = data["first_name"]
			checkuser.last_name = data["last_name"]
			checkuser.phone = str(data["phone"]) 
			checkuser.date_updated  = constants.FORMATTED_TIME()
			db.session.commit()
			content = {
	               'status' : 1,	
	               'message' : 'Success'
	               }
		else:
			print "else"
			content = {
						'status' : 0,	
			            'message' : 'Invalid data'
			        	}
		
		if Address:
			print "address"
			if data["zipcode"]=='':
					Address.zipcode = 0
			else:
				Address.zipcode =str(data["zipcode"]) 
				print "here"
			Address.user_id =str(data["_id"])
			Address.address_line1  = str(data["address_line1"])
			Address.address_line2  = str(data["address_line2"])
			Address.appartment  = str(data["appartment"])
			Address.city  = data["city"]
			Address.state =  data["state"]
			Address.company  = data["company"]
			Address.date_created  = constants.FORMATTED_TIME()
			Address.date_updated  = constants.FORMATTED_TIME()
			db.session.commit()	
			
			content = {
	               'status' : 1,	
	               'message' : 'Success'
	               }

		else :
			print "address2"
			userObj = UserAddress(data["_id"],str(data["address_line1"]),str(data["address_line2"]),str(data["appartment"]),unicode(data["city"]),unicode(data["state"]),int(data["zipcode"]),unicode(data["company"]))
			db.session.add(userObj)
			db.session.commit()
	     	content = {
						'status' : 1,	
			            'message' : 'Success'
			        	}

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)


 # Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/verifyToken/', methods=['POST'])
def verifyToken():
	content = {
               'status' : 0,	
               'message' : 'Invalid Token',
               }
	data = request.json
	try:
		# userObj = Users.query.filter_by(id=data["user_id"]).first()
		print "here"
		getToken = Token.query.filter_by(token=str(data["token"])).first()
		if getToken:
			print "some"
			userObj = Users.query.filter_by(id=getToken.user_id).first()
			if userObj:
				print "pass"
				print userObj.email
				userObj.status = 1
				userObj.date_updated = constants.FORMATTED_TIME()
				print constants.FORMATTED_TIME()
				print userObj.date_updated
				db.session.commit()
			content = {
	               'status' : 1,	
	               'message' : 'Success',
	               'address_id' : str(userObj.id),
	               'key':data["token"]
	               }
		# else:
		# 	new = Token(data["token"])
	 #    	db.session.add(new)
	 #    	db.session.commit()
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)
	
# Login In  API Ends
# Return json with the status of login

@user_link.route('/forgot/', methods=['POST'])
def Forgot():
	responseData={}
	content={
			'status':0,
			'message':'Invalid Call'
			}

	data=request.json
	
	try:
			user = Users.query.filter_by(email=str(data["email"])).first()
			# responseData["email"]=user.email
			
			if user:
				print "HI"
				content={
					'status':1,
					'message':'Success'
					}
				token = str(uuid.uuid4()).replace("-","")	
				tokenObj = Token(token,user.id)
	 	    	db.session.add(tokenObj)
	    	 	db.session.commit()

	    	 	tokenObj.date_created = constants.FORMATTED_TIME()
	    	 	tokenObj.date_updated = constants.FORMATTED_TIME()
	    	 	db.session.commit()
	    	 	url = urllib.urlopen(os.path.join(APP_STATIC, 'reset.html'))
	    	 	html = url.read()
	    	 	url.close()
	    	 	subject = "Link for Password Reset"
	    	 	html = string.replace(html, "[token]", tokenObj.token)
	    	 	print (tokenObj.token)
	    	 	res = functions.sendEmail("help@healora.com",user.email,subject,html,"")

    			
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
		content['data'] = responseData
	return jsonify(content)	


@user_link.route('/reset/', methods=['POST'])
def reset():
	content = {
               'status' : 0,	
               'message' : 'Invalid Token',
               }
	data = request.json
	try:
		getToken = Token.query.filter_by(token=str(data["token"])).first()
		if getToken:
			a=constants.diffInDates(getToken.date_created)
			user_id = getToken.user_id
			print getToken.id
			if a<=600:
				print a
				passwrd = hashlib.md5(str(data["password"]).encode())
				userObj = Users.query.filter_by(id=user_id).first()
				userObj.password =passwrd.hexdigest()
				userObj.status =1
				userObj.date_updated =constants.FORMATTED_TIME()
				db.session.commit()

				
				content = {
		               'status' : 1,	
		               'message' : 'Success',
		               'address_id' : str(userObj.id)
		               }
			else:
				content = {
		               'status' : 0,
		               'message' : 'Session timeout, Please try again'
		               }

			Token.query.filter(Token.user_id == user_id).delete()
			db.session.commit()

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)

 # Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/sendAllEmail/', methods=['POST'])
def sendAllEmail():
	content = {
               'status' : 0,	
               'message' : "Failure"
               }
	data = request.json
	htmlSource=""
	html=""
	html=html+"<div>We have recieved data from page named: "+data["page"]+" </div></br><div class='clr'>&nbsp;</div><div class='clr'>&nbsp;</div></br></br>"
	try:
		for key,value in sorted(data["data"].items()):
			print key
			html=html+'<i><div>'+key+': '+value+'</div></i></br>'
		content = {
               'status' : 1,	
               'message' : "Success"
               }
		sock = urllib.urlopen(os.path.join(APP_STATIC, 'contact_in.html'))
		htmlSource = sock.read()
		sock.close()
		htmlSource = string.replace(htmlSource, "[html]", html)
		res = functions.sendEmail("help@healora.com",data["email"],data["subject"],htmlSource,"")
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)
	
# Login In  API Ends
# Return json with the status of login



# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/userAddress/', methods=['POST'])
def userAddress():
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	data = request.json
	getUser = Users.query.filter_by(id=data["user_id"]).first()
	print "here"
	userObj = UserAddress(getUser.id,str(data["address_line1"]),str(data["address_line2"]),str(data["appartment"]),data["city"],data["state"],data["zipcode"],data["company"])
	db.session.add(userObj)
	db.session.commit()
	
	userObj.date_created = constants.FORMATTED_TIME()
	userObj.date_updated = constants.FORMATTED_TIME()
	db.session.commit()
	content = {
               'status' : 1,	
               'message' : 'Success',
               'address_id' : str(userObj.id)
               }
    
	return jsonify(content)
	
# Login In  API Ends
# Return json with the status of login


# Blog   API starts
# Takes blog id  
# Set the route and accepted methods
@user_link.route('/addBlog/', methods=['POST'])
def addBlog():
	data = request.json
	print "fads"
	blog = Blog(str(data["name"]) ,str(data["podcast"]) ,str(data["short_name"]) ,str(data["short_text"].encode('utf-8').strip()) ,str(data["url"]) ,str(data["category"]), str(data["tags"]),str(data["image"]) ,str(data["image_detail"])  ,str(data["user_type"]) ,str(data["created_by"]) ,str(data["desc"].encode('utf-8').strip()) ,1)
	db.session.add(blog)
	db.session.commit()
	
	blog.date_created = constants.FORMATTED_TIME()
	blog.date_updated = constants.FORMATTED_TIME()
	db.session.commit()

	content = {
               'status' : 1,	
               'message' : 'Success',
               'address_id' : str(blog.id)
               }
    
	return jsonify(content)
	
# Blog  API Ends
# Return json with the status of login

# Blog   API starts
# Takes blog id  
# Set the route and accepted methods
@user_link.route('/blogList/', methods=['POST'])
def blogList():
	data = request.json
	blogs = []
	responseData={}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	try:
		getBlog = Blog.query.order_by(desc("date_created")).all()
		# getBlog = db.Blog.find({}).sort([("date_created", -1)])
		for x in getBlog:
			blogs.append({
					"name":x.name,
					"short_name":x.short_name,
					"short_text":x.short_text,
					"podcast":x.podcast,
					"id":str(x.id),
					"url":x.url,
					"tags":x.tags,
					"image":x.image,
					"created_by":x.created_by,
					"date_created":x.date_created
					})
		print constants.FORMATTED_TIME()
		responseData['list']=blogs
		content = {
	               'status' : 1,	
	               'message' : 'Success',
	               }
	except Exception as e:
		print e
	content["data"] = responseData    
	return jsonify(content)
	
# Blog  API Ends
# Return json with the status of login



# Blog   API starts
# Takes blog id  
# Set the route and accepted methods
@user_link.route('/blogDetail/', methods=['POST'])
def blogDetail():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call'
               }
	data = request.json
	try:
		getBlog = Blog.query.filter_by(url= str(data["url"])).first()
		# responseData["name"] = str(getBlog.name)
		responseData["short_name"] = str(getBlog.short_name)
		responseData["short_text"] = str(getBlog.short_text.encode("utf8"))
		responseData["tags"] = str(getBlog.tags)
		responseData["podcast"] = str(getBlog.podcast)
		responseData["image"] = str(getBlog.image)
		responseData["created_by"] = str(getBlog.created_by)
		responseData["desc"] = str(getBlog.desc.encode("utf8"))
		responseData["date_created"] = str(getBlog.date_created)
		content = {
	               'status' : 1,	
	               'message' : 'Success'
	               }
	except Exception as e:
		print e
	content["data"] = responseData   
	return jsonify(content)
	
# Blog  API Ends
# Return json with the status of login



# Blog   API starts
# Takes blog id  
# Set the route and accepted methods
@user_link.route('/addComment/', methods=['POST'])
def addComment():
	data = request.json
	getUser = Users.query.filter_by(id=data["user_id"]).first()
	print "here"

	if getUser:
		print "found"
	userObj = Comment(str(data["url"]),str(data["desc"]),str(getUser.first_name+" "+getUser.last_name),getUser.id,1)
	db.session.add(userObj)
	db.session.commit()

	userObj.date_created = constants.FORMATTED_TIME()
	db.session.commit()
	print userObj

	content = {
               'status' : 1,	
               'message' : 'Success',
               'address_id' : str(userObj.id),
               'key': str(userObj.blog_url)
               }
    
	return jsonify(content)
	
# Blog  API Ends
# Return json with the status of login



# Blog   API starts
# Takes blog id  
# Set the route and accepted methods

@user_link.route('/getComment/', methods=['POST'])
def getComment():
	responseData=[]
	content = {
               'status' : 0,	
               'message' : 'Invalid Call'
               }
	data = request.json
	try:
		getComment = Comment.query.filter_by(blog_url=data["url"]).all()
		# print getComment.count()
		if getComment:
			for x in getComment:
				responseData.append({ "desc" : x.desc,
								        "name" : x.name,
								        "status" : x.status,
								        "image" : "doctor.png",
								        "date_created" : x.date_created})
		content = {
	               'status' : 1,	
	               'message' : 'Success'
	               }
	except Exception as e:
		print e
	content["data"] = responseData   
	return jsonify(content)
	
# Blog  API Ends
# Return json with the status of login


# Register In  API starts
# Takes username detail and register 
# Set the route and accepted methods

@user_link.route('/docRegister/', methods=['POST'])
def docRegister():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	data = request.json
	# # Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"docRegister",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #
	try:
		if validData==True:
			check = Doctor.query.filter_by(email=str(data["email"])).first()

			if check:
				content = {
			               'status' : 0,	
			               'message' : 'User Already Exit.'
			               }
			else:
				password = hashlib.md5(str(data["password"]).encode())
				docObj = Doctor(data["first_name"], data["last_name"],str(data["email"]),str(data["first_name"]+" "+data["last_name"])  ,password.hexdigest(),str(data["phone"]) ,str(data["fax"]) ,str(data["image"]) ,str(data["degree_title"]),1)
				db.session.add(docObj)
				db.session.commit()
			
				docObj.date_created = constants.FORMATTED_TIME()
				docObj.date_updated = constants.FORMATTED_TIME()
				db.session.commit()

				responseData["user_id"] = str(docObj.id)
				responseData["user_name"] = docObj.user_name
				responseData["first_name"] = docObj.first_name
				responseData["degree_title"] = docObj.degree_title
				responseData["email"] = docObj.email

				rand = ''
				rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(12)])
				token = str(rand)
				tokenObj= Token(token,docObj.id)
				
				db.session.add(tokenObj)
	    		db.session.commit()


	    		tokenObj.date_created = constants.FORMATTED_TIME()
	    		tokenObj.date_updated = constants.FORMATTED_TIME()
	    		db.session.commit()

	    		sock = urllib.urlopen(os.path.join(APP_STATIC, 'register.html'))
	    		htmlSource = sock.read()
	    		sock.close()
	    		subject = "Welcome to Healora."
	    		htmlSource = string.replace(htmlSource, "[token]", str(rand))

	    		res = functions.sendEmail("help@healora.com",docObj.email,subject,htmlSource,"")
	    		content = {
			               'status' : 1,	
			               'message' : 'Success',
			               'user_id' : str(docObj.id)
			               }
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content['data'] = responseData
	return jsonify(content)
# Register In  API Ends
# Return json with the status of login

@user_link.route('/docLogin/', methods=['POST'])
def docLogin():
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"doclogin",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #	
	try:
		if validData==True:
			password = hashlib.md5(str(data['password']).encode())
			getDoc = Doctor.query.filter_by(email=str(data["user_name"]),password = password.hexdigest()).first()
			if getDoc:
				responseData["user_id"] = str(getDoc.id)
				responseData["user_name"] = getDoc.user_name
				responseData["first_name"] = getDoc.first_name
				responseData["degree_title"] = getDoc.degree_title
				responseData["email"] = getDoc.email
				content["status"] = 1
				content["message"] = msgtext.success
			else:
				getDocByPhone = Doctor.query.filter_by(phone=str(data["user_name"]),password = password.hexdigest()).first()

				if getDocByPhone:
					responseData["user_id"] = str(getDocByPhone.id)
					responseData["user_name"] = getDocByPhone.user_name
					responseData["first_name"] = getDocByPhone.first_name
					responseData["degree_title"] = getDoc.degree_title
					responseData["email"] = getDocByPhone.email
					content["status"] = 1
					content["message"] = msgtext.success
				else:
					content["status"] = 0
					content["message"] = "User/Password Does not matched"
	except Exception as e:
		print e
	content["data"] = responseData
	content["message"] = content["message"].replace("_"," ")
	return jsonify(content)
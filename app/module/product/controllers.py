__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify, Response


# Import the database object from the main app module
from app import db, constants, functions, APP_STATIC
import json, urllib, datetime, email, re, hashlib, random, string, urllib, os, pdfkit
from bs4 import BeautifulSoup
from app.model import model
from app.model.model import *
# Speciality,Users,Doctor,DoctorAddress, DoctorSpeciality, DoctorCodes, BodyPart, Category, Procedure,ProcedureDetail, DoctorProcedure, Order
from bson.objectid import ObjectId
from app.module.validation import validate, msgtext

# Define the blueprint: 'auth', set its url prefix: app.url/auth
product_link = Blueprint('product', __name__, url_prefix='/product')


# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/testing/', methods=['POST'])
def testing():
	print "enter"
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		content = {
               'status' : 1,
               'message' : 'Success',
               }
	except Exception as e:
		print e
	content["data"] = responseData
	print content
	return jsonify(content) 

# Login In  API Ends
# Return json 




# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getListing/', methods=['POST'])

def getListing():
	data = request.json
	responseData = {}
	bodyParts = []
	category = []
	procedure = []
	catProcedure = []
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		getBodyParts = BodyPart.query.order_by("name").all()
		# print getBodyParts
		# getBodyParts = db.BodyPart.find().sort([("name", 1)])
		for x in getBodyParts:
			bodyParts.append({"id":str(x.id),"name":x.name})
		
		getCategory = Category.query.order_by("name").all()
		# getCategory = db.Category.find().sort([("name", 1)])
		for y in getCategory:
			catProcedure = []
			getCatProcedure = Procedure.query.filter_by(category=y.id).order_by("name").all()
			# getBodyParts = BodyPart.query.filter_by(category=y.id).order_by("name").all()
			# getCatProcedure=db.Procedure.find({"category":ObjectId(str(y.id))}).sort([("name", 1)])
			for p in getCatProcedure:
				catProcedure.append({"id":str(p.id),"name":p.name,"category":str(p.category),"short_name":p.short_name})
			# getBodyName = db.BodyPart.find_one({"_id":ObjectId(y.body_part)})
			category.append({
				"id":str(y.id),
				"name":y.name,
				"short_name":y.short_name,
				"speciality":str(y.speciality),
				"procudure":catProcedure,
				"body_part":str(y.body_part)
				})

		responseData["bodyParts"] = bodyParts
		responseData["category"] = category
		# responseData["procedure"] = procedure
		content["status"] = 1
		content["message"] = msgtext.success
	except Exception as e:
		print e
	content["data"] = responseData
	# print content
	return jsonify(content)

# Login In  API Ends
# Return json 



# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getProcedure/', methods=['POST'])
def getProcedure():
	data = request.json
	responseData = {}
	procedure = []
	image=""
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		getSpeciality = Speciality.query.filter_by(name = str(data["speciality"])).first()
		print getSpeciality
		# getSpeciality = db.Speciality.find_one({"name":str(data['speciality'])})
		getCategory = Category.query.filter_by(speciality=getSpeciality.id).all()
		# getCategory = db.Category.find({"speciality":getSpeciality.id})
		for cat in getCategory:
			image=cat.image
			getProcedure = Procedure.query.filter_by(category=cat.id).all()
			# getProcedure=db.Procedure.find({"category":cat.id})
			for z in getProcedure:
				procedure.append({"id":str(z.id),"name":z.name,"category":str(z.category),"short_name":z.short_name})
		responseData["procedure"] = procedure
		responseData["image"] = image
		content["status"] = 1
		content["message"] = msgtext.success
	except Exception as e:
		print e
	content["data"] = responseData
	return jsonify(content)
	# return Response(content, mimetype='application/json')	

# Login In  API Ends
# Return json 


# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getAverageQuote/', methods=['POST'])
def getAverageQuote():
	data = request.json
	responseData = {}
	Detail=[]
	calcPrice = 0
	count = 0
	averagePrice = ""
	insaurancePrice = ""
	doctorPriceList = []
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# try:
	getProcDetail = Procedure.query.filter_by(id = data['procedure']).first()
	# getProcDetail = db.Procedure.find_one({"_id":ObjectId(data['procedure'])})
	getCategory = Category.query.filter_by(id = getProcDetail.category).first()
	# getCategory = db.Category.find_one({"_id":getProcDetail.category})
	getDocPrice = DoctorProcedure.query.filter_by(procedure = data['procedure']).all()
	# print getDocPrice
	# getDocPrice = db.DoctorProcedure.find({"procedure":ObjectId(data['procedure'])})
	# if getDocPrice.count() != 0:
	if getDocPrice:
		for x in getDocPrice:
			print x.doctor
			count= count +1
			calcPrice = float(calcPrice) + float(x.total)
			getDoc = Doctor.query.filter_by(id = x.doctor,status = 1).first()
			# getDoc = db.Doctor.find_one({"_id":ObjectId(x.doctor),"status":1})
			getDocSpec = DoctorSpeciality.query.filter_by(doctor_id = x.doctor).first()
			# getDocSpec = db.DoctorSpeciality.find_one({"doctor_id":ObjectId(x.doctor)})
			if getDoc is not None:
				doctorPriceList.append({"doctor_id":str(x.doctor),
										"doctor_name":str(getDoc.first_name)+" "+str(getDoc.last_name),
										"doctor_image":str(getDoc.image),
										"doctor_proc_id":str(x.id),
										"doctor_price":str("{:,.2f}".format(float(x.total))),
										"doctor_speciality":str(getDocSpec.speciality_name),
										"degree_title":str(getDoc.degree_title)
										})
		responseData["averagePrice"] = "0"
		content["status"] = 1
		if calcPrice >= 1:
			averagePrice = round(calcPrice/count,2)
			responseData["averagePrice"] = str("{:,.2f}".format(float(averagePrice)))
			responseData["saving"] = str(float(getProcDetail.insurance_price) - averagePrice)
			responseData["savingPercent"] = str(round(float(responseData["saving"])/float(getProcDetail.insurance_price)*100.0,2))
			responseData["saving"] = str("{:,.2f}".format(float(responseData["saving"])))
			# print (abs(float(getProcDetail.insurance_price) - float(responseData["saving"])))/float(responseData["saving"])*100.0
		responseData["doctorPriceList"] = doctorPriceList
		responseData["insaurancePrice"] = str("{:,.2f}".format(float(getProcDetail.insurance_price)))
		getDetail = ProcedureDetail.query.filter_by(procedure_id = data['procedure']).all()
		print getDetail
		for x in getDetail:
			Detail.append({"data":x.detail})
		responseData["detail"] = Detail
		responseData["sku"] = getProcDetail.sku
		responseData["name"] = getProcDetail.name
		responseData["shortName"] = getProcDetail.short_name
		responseData["desc"] = getProcDetail.desc
		responseData["cpt"] = getProcDetail.cpt
		
		responseData["image"] = getCategory.image
		responseData["catName"] = getCategory.name
		content["message"] = msgtext.success
	# except Exception as e:
	# 	print e
	content["data"] = responseData
	return jsonify(content)

# Login In  API Ends
# Return json 



# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getDoctorProcedure/', methods=['POST'])
def getDoctorProcedure():
	data = request.json
	responseData = {}
	procedure = []
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		getDoc = Doctor.query.filter_by(id = data['doctor_id']).first()
		print "yes"
		# getDoc = db.Doctor.find_one({"id":ObjectId(data["doctor_id"])})
		if getDoc is not None:
			getDocSpec = DoctorSpeciality.query.filter_by(doctor_id = getDoc.id).first()
			getDocAdd = DoctorAddress.query.filter_by(doctor_id = getDoc.id).first()
			doctorDetail={"doctor_id":str(getDoc.id),
						"doctor_name":str(getDoc.first_name)+" "+str(getDoc.last_name),
						"doctor_image":str(getDoc.image),
						"doctor_speciality":str(getDocSpec.speciality_name),
						"degree_title":str(getDoc.degree_title),
						"address1":str(getDocAdd.address_line1),
						"address2":str(getDocAdd.address_line2),
						"appartment":str(getDocAdd.appartment),
						"city":str(getDocAdd.city),
						"state":str(getDocAdd.state),
						"zip":str(getDocAdd.zipcode)
						}
			getDocProcedure = DoctorProcedure.query.filter_by(doctor = data['doctor_id']).all()
			for cat in getDocProcedure:
				if cat.procedure is not None:
					getProcedure = Procedure.query.filter_by(id = cat.procedure).first()
					print cat.procedure
					# getProcedure=db.Procedure.find_one({"_id":cat.procedure})
					procedure.append({"id":str(cat.id),"name":getProcedure.name})
				
			responseData["procedure"] = procedure
			responseData["doctordetail"] = doctorDetail
			content["status"] = 1
			content["message"] = msgtext.success
	except Exception as e:
		print e
	content["data"] = responseData
	return jsonify(content)
	# return Response(content, mimetype='application/json')	

# Login In  API Ends
# Return json 



# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getDoctors/', methods=['POST'])
def getDoctors():
	data = request.json
	responseData = {}
	doc=[]
	ids=[]
	calcPrice = 0
	count = 0
	doctorList = []
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# try:
	if data['last_name'].strip() != "":
		# getDoc = db.Doctor.find({"last_name":{"$regex": data['last_name'].strip()}})
		regex = ".*" + data['last_name'].strip() + ".*";
		# SELECT name FROM person_tbl WHERE name REGEXP 'mar';
		# print re.compile(regex, re.IGNORECASE)

		getDoc=Doctor.query.filter_by().all()
		# print getDoc
		for x in getDoc:
			# 
			# print x.last_name
			doc.append(x.last_name)

		# getDoc = Doctor.query.filter_by(last_name = re.compile(regex, re.IGNORECASE)).all()
		# last_name=re.compile(regex, re.IGNORECASE)
		print ids
		
		# getDoc = db.Doctor.find({"last_name": re.compile(regex, re.IGNORECASE)})
		# getDoc = db.Doctor.find({"last_name":"monga"})
		for i in doc:
			R=re.findall(regex,i, re.IGNORECASE)
			if R:
				getLDoc = Doctor.query.filter_by(last_name = i).all()
				# print getLDoc
		# if getDoc:

				for x in getLDoc:
					
					getDocSpec = DoctorSpeciality.query.filter_by(doctor_id = x.id).first()
					# getDocSpec = db.DoctorSpeciality.find_one({"doctor_id":ObjectId(x.id)})
					if x.id not in ids:
						ids.append(x.id)
						if getDocSpec is not None:
							doctorList.append({"doctor_id":str(x.id),
													"doctor_name":str(x.first_name)+" "+str(x.last_name),
													"doctor_image":str(x.image),
													"doctor_speciality":str(getDocSpec.speciality_name),
													"degree_title":str(x.degree_title)
													})
				content["status"] = 1
				responseData["doctorList"] = doctorList
				content["message"] = msgtext.success
	# except Exception as e:
	# 	print e
	content["data"] = responseData
	return jsonify(content)

# Login In  API Ends
# Return json 




# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getFinalPrice/', methods=['POST'])
def getFinalPrice():
	data = request.json
	responseData = {}
	Detail=[]
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		x = DoctorProcedure.query.filter_by(id = data['doctorProcedureId']).first()
		# print x
		# x = db.DoctorProcedure.find_one({"_id":ObjectId(data['doctorProcedureId'])})
		if x:
			getDoc = Doctor.query.filter_by(id = x.doctor).first()
			# getDoc = db.Doctor.find_one({"_id":ObjectId(x.doctor)})
			getDocAdd = DoctorAddress.query.filter_by(doctor_id = x.doctor).first()
			# getDocAdd = db.DoctorAddress.find_one({"doctor_id":ObjectId(x.doctor)})
			getDocSpec = DoctorSpeciality.query.filter_by(doctor_id = x.doctor).first()
			# getDocSpec = db.DoctorSpeciality.find_one({"doctor_id":ObjectId(x.doctor)})
			rPrice = float(0.18)*float(x.doctor_price)
			rPrice = round(rPrice,2)
			responseData={"doctor_id":str(x.doctor),
									"doctor_name":str(getDoc.first_name)+" "+str(getDoc.last_name),
									"doctor_image":str(getDoc.image),
									"doctor_proc_id":str(x.id),
									"total":str(x.total),
									"showtotal":str("{:,.2f}".format(float(x.total))),
									"reservePrice":str(rPrice),
									"showreservePrice":str("{:,.2f}".format(float(rPrice))),
									"balance":str(float(x.total)-rPrice),
									"showbalance":str("{:,.2f}".format(float(x.total)-rPrice)),
									"doctor_price":str(x.doctor_price),
									"showdoctor_price":str("{:,.2f}".format(float(x.doctor_price))),
									"doctor_speciality":str(getDocSpec.speciality_name),
									"degree_title":str(getDoc.degree_title),
									"doctor_title":"Dr.",
									"address1":str(getDocAdd.address_line1),
									"address2":str(getDocAdd.address_line2),
									"appartment":str(getDocAdd.appartment),
									"city":str(getDocAdd.city),
									"state":str(getDocAdd.state),
									"zip":str(getDocAdd.zipcode)
									}
			getProcDetail = Procedure.query.filter_by(id = x.procedure).first()
			# getProcDetail = db.Procedure.find_one({"_id":ObjectId(x.procedure)})
			getCategory = Category.query.filter_by(id = getProcDetail.category).first()
			# getCategory = db.Category.find_one({"_id":getProcDetail.category})
			content["status"] = 1
			responseData["insaurancePrice"] = getProcDetail.insurance_price
			getDetail = ProcedureDetail.query.filter_by(procedure_id = x.procedure).all()
			print type(getDetail)
			print getDetail
			for x in getDetail:
				print x
				Detail.append({"data":x.detail})
			responseData["proc_detail"] = Detail
			responseData["sku"] = getProcDetail.sku
			responseData["name"] = getProcDetail.name
			responseData["shortName"] = getProcDetail.short_name
			responseData["desc"] = getProcDetail.desc
			responseData["cpt"] = getProcDetail.cpt
			responseData["image"] = getCategory.image
			responseData["catName"] = getCategory.name
			content["message"] = msgtext.success
	except Exception as e:
		print e
	content["data"] = responseData
	return jsonify(content)

# Login In  API Ends
# Return json 



# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getBlog/', methods=['POST'])
def getBlog():
	data = request.form
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	responseData = {
					"blogList":[{
					"name":"name testing",
					"image":"",
					"short_desc":"this is tesing ",
					"id":"12345",
					"created_by":"testt",
					"tags":"testsing tags",
					"date_created": "2018-03-02 10:10:10",
					"day_created": "02",
					"month_created": "03"
					},
					{
					"name":"name fasdfadsf",
					"image":"",
					"short_desc":"asdfsaf is tesing ",
					"id":"123456",
					"created_by":"testt",
					"tags":"testsing fafsdfa",
					"date_created": "2018-03-05 10:10:10",
					"day_created": "05",
					"month_created": "03",
					}]
	}
	content["message"] = msgtext.success
	content["data"] = responseData
	return jsonify(content)



# Login In  API Ends
# Return json 

# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/placeOrder/', methods=['POST'])
def placeOrder():
	data = request.json
	rand = ''
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	responseData={}
	try:
		if data["temp"]=='1':
			rand = getRand()
			
			odrObj = Order(data['user_id'],data['doctor_procedure'],str(rand),"1","paypal","","","","2","",str(data["total"]),str(data["balance"]),str(data["reservePrice"]),"payment started ")
			db.session.add(odrObj)
			db.session.commit()
			

			responseData['order_id']=rand
			responseData['placeOrderId']=str(odrObj.id)
		elif data["temp"]=='2':
		

			orderUpdate = Order.query.filter_by(id=data['placeOrderId']).first()
			
			orderUpdate.payment_id  = data["payment_id"]
			orderUpdate.request = data["request"]
			orderUpdate.temp = str(2) 
			orderUpdate.date_updated  = constants.FORMATTED_TIME()
			db.session.commit()
			
			
			responseData['message']="Updated"
		
		elif data["temp"]=='3':
			orderUpdate = Order.query.filter_by(id=data['payment_id']).first()
			
			orderUpdate.payment_id  = data["payment_id"]
			orderUpdate.responce = data["responce"]
			orderUpdate.temp = str(3) 
			orderUpdate.date_updated  = constants.FORMATTED_TIME()
			db.session.commit()

			responseData['message']="Updated"
		elif data["temp"]=='4':
	
			orderUpdate = Order.query.filter_by(id=data['payment_id']).first()
			
			orderUpdate.responce_execute  = data["responce"]
			orderUpdate.status = data["status"]
			orderUpdate.temp = str(0)
			orderUpdate.comments = str(data['comments'])
			orderUpdate.date_updated  = constants.FORMATTED_TIME()
			db.session.commit()

			responseData['message']="Updated"
			
			docProced = DoctorProcedure.query.filter_by(id = orderUpdate.doctor_procedure).first()
			getDoctor = Doctor.query.filter_by(id = docProced.doctor).first()
			getProcedure = Procedure.query.filter_by(id = docProced.procedure).first()
			userObj = Users.query.filter_by(id = orderUpdate.user).first()
			doc_name="Dr. "+getDoctor.first_name+" "+getDoctor.last_name
			if userObj.email != "":
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'purchase.html'))
				htmlSource = sock.read()
				sock.close()
				subject = "Your Purchase is Completed"
				htmlSource = string.replace(htmlSource, "[DOCTOR]", doc_name)
				htmlSource = string.replace(htmlSource, "[PROCEDURE]", getProcedure.name)
				htmlSource = string.replace(htmlSource, "[total]", str("{:,.2f}".format(float(orderUpdate.total_amount))))
				htmlSource = string.replace(htmlSource, "[balance]", str("{:,.2f}".format(float(orderUpdate.balance))))
				htmlSource = string.replace(htmlSource, "[paid]", str("{:,.2f}".format(float(orderUpdate.reserve_price))))
				htmlSource = string.replace(htmlSource, "[orderid]", orderUpdate.order_id)
				pdfname=str(data['payment_id'])+'.pdf'
				pdfkit.from_string(htmlSource,'app/media/'+pdfname)
				res = functions.sendEmail("help@healora.com","k.m.developer0078@gmail.com",subject,htmlSource,"media/"+pdfname);
				# res = functions.sendEmail("help@healora.com",userObj.email,subject,htmlSource,"");
		else:
			pass
		
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content["data"] = responseData
	return jsonify(content)


def getRand():
	rand = ''
	rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(9)])
	checkOid = Order.query.filter_by(order_id = str(rand)).first()
	# checkOid=db.Order.find_one({"order_id":str(rand)})
	if checkOid:
		getRand()
	else:
		return rand
# Login In  API Ends
# Return json 

# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getOrder/', methods=['POST'])
def getOrder():
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		orderObj = Order.query.filter_by(payment_id = data['payment_id']).first()
		docProcObj = DoctorProcedure.query.filter_by(id = orderObj.doctor_procedure).first()
		procObj = Procedure.query.filter_by(id = docProcObj.procedure).first()
		getDoc = Doctor.query.filter_by(id = docProcObj.doctor).first()
		getDocAdd = DoctorAddress.query.filter_by(doctor_id = getDoc.id).first()


		responseData["reserve_price"] = str("{:,.2f}".format(float(orderObj.reserve_price)))
		responseData["doctor_name"] = getDoc.first_name+" "+getDoc.last_name
		responseData["phone"] = getDoc.phone
		responseData["fax"] = getDoc.fax
		responseData["title"] = 'Dr.'
		responseData["balance"] = str("{:,.2f}".format(float(orderObj.balance)))
		responseData["total_amount"] = str("{:,.2f}".format(float(orderObj.total_amount)))
		responseData["name"] = procObj.name
		responseData["address1"] = str(getDocAdd.address_line1)
		responseData["address2"]=str(getDocAdd.address_line2)
		responseData["appartment"]=str(getDocAdd.appartment)
		responseData["city"]=str(getDocAdd.city)
		responseData["state"]=str(getDocAdd.state)
		responseData["zip"]=str(getDocAdd.zipcode)

		responseData["date_created"] = constants.FORMATTED_DATE_ONLY(orderObj.date_created)
		content['status'] = 1
		content["message"] = msgtext.success
	except Exception as e:
		print e
	content["data"] = responseData
	return jsonify(content)

# Login In  API Ends
# Return json 


# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getOrderHistory/', methods=['POST'])
def getOrderHistory():
	data = request.json
	orderData=[]
	responseData = {}
	status = ""
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		orderObj = Order.query.filter_by(user = data['user_id']).all()
		# orderObj = db.Order.find({"user":ObjectId(data['user_id'])})
		for x in orderObj:
			userObj = Users.query.filter_by(id = data['user_id']).first()
			# userObj = db.Users.find_one({"_id":ObjectId(data['user_id'])})

			content["name"] = userObj.first_name+" "+userObj.last_name
			content["email"] = userObj.email
			content["phone"] = str(userObj.phone)
			print x.status
			if x.status=="0" or x.status=="1":
				status="Approved"
				if x.status=="0":
					status="Declined"
				docProcObj = DoctorProcedure.query.filter_by(id = x.doctor_procedure).first()
				# docProcObj = db.DoctorProcedure.find_one({"_id":ObjectId(x.doctor_procedure)})
				procObj = Procedure.query.filter_by(id = docProcObj.procedure).first()
				# procObj = db.Procedure.find_one({"_id":ObjectId(docProcObj.procedure)})
				catObj = Category.query.filter_by(id = procObj.category).first()
				# catObj = db.Category.find_one({"_id":ObjectId(procObj.category)})
				specObj = Speciality.query.filter_by(id = catObj.speciality).first()
				# specObj = db.Speciality.find_one({"_id":ObjectId(catObj.speciality)})
				getDoc = Doctor.query.filter_by(id = docProcObj.doctor).first()
				# getDoc = db.Doctor.find_one({"_id":ObjectId(docProcObj.doctor)})

				orderData.append({"total_amount":str("{:,.2f}".format(float(x.total_amount)))
					,"name":procObj.name
					,"date_created":constants.FORMATTED_DATE_ONLY(x.date_created)
					,"status":status
					,"speciality":specObj.name
					,"degree_title":getDoc.degree_title
					,"doctor_name":getDoc.first_name+" "+getDoc.last_name
					,"balance":str("{:,.2f}".format(float(x.balance)))
					,"reserve_price":str("{:,.2f}".format(float(x.reserve_price)))
					,"order_id":x.order_id
					,"order_id":x.order_id
					})
		responseData["orderData"] = orderData
		content["message"] = msgtext.success
		content["status"] = 1
	except Exception as e:
		print e
	content["data"] = responseData
	return jsonify(content) 

# Login In  API Ends
# Return json 



# get data of procedure list returns all procecure details  API starts
# Set the route and accepted methods
@product_link.route('/getOrderPrice/', methods=['POST'])
def getOrderPrice():
	data = request.json
	orderData=[]
	responseData = {}
	status = ""
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	try:
		orderObj = Order.query.filter_by(id = data['id']).first()
		# orderObj = db.Order.find_one({"_id":ObjectId(data['id'])})
		content["amount"] = ""
		if orderObj:
			content["amount"] = orderObj.reserve_price
		content["message"] = msgtext.success
		content["status"] = 1
	except Exception as e:
		print e
	# content["data"] = responseData
	return jsonify(content) 

# Login In  API Ends
# Return json 





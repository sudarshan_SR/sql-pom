__author__ = 'Kunal Monga'

checkList={}
# Required params:
# 1- its required and mandatory
# 2- Its not mandatory
# 3- Required if parent exist
# 
# 
checkList['register']={
						"email":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkEmailUnique']
							},
						"first_name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"last_name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"password":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"phone":{ 
								"required":2,
								"parent":"",
								"function":["checkIntIfNotNull", 'checkPhoneUnique']
							},
						"promocode":{ 
								"required":2,
								"parent":"",
								"function":["notEmpty"]
							}
					}


checkList['login']={
						"user_name":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty']
							},
						"password":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}


checkList['docRegister']={
						"email":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkDocEmailUnique']
							},
						"first_name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"last_name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"password":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"phone":{ 
								"required":2,
								"parent":"",
								"function":["checkIntIfNotNull", 'checkDocPhoneUnique']
							},
						"fax":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"degree_title":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}

checkList['doclogin']={
						"user_name":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty']
							},
						"password":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}
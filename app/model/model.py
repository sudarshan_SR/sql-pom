from app import app, db
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime



class Users(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(80), unique=True, nullable=False)
    first_name = db.Column(db.String(120))
    last_name = db.Column(db.String(120))
    user_name = db.Column(db.String(120))
    password = db.Column(db.String(120))
    phone = db.Column(db.String(80), unique =True)
    status = db.Column(db.Integer,  nullable = True)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    token = db.relationship('Token', backref='id_users', lazy=True)
    user_address = db.relationship('UserAddress', backref='id_users', lazy=True)
    comment = db.relationship('Comment', backref='id_users', lazy=True)
    order = db.relationship('Order', backref='id_users', lazy=True)



    def __init__(self,email,first_name,last_name,username,password,phone):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.user_name = username
        self.password = password
        self.phone = phone
        self.status = 1

    def __repr__(self):
        return '<Users %r>' % self.user_name
        # return '<Users {}>'.format(self.username)

class Token(db.Model):
    __tablename__ = 'token'
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),nullable=False)
    # doctor_id = db.Column(db.Integer, db.ForeignKey('doctor.id'),nullable=False)
    token = db.Column(db.String(120))
    date_created = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
     

    def __init__(self,token,user_id):
        self.token = token
        self.user_id = user_id
        # self.doctor_id = doctor_id
        # self.date_created = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
        # self.date_updated = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)        

    def __repr__(self):
        return '<Token %r>' % self.token

 

# User address details 
class UserAddress(db.Model):
    __tablename__ = 'user_address'

    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    address_line1 = db.Column(db.String(120))
    address_line2 = db.Column(db.String(120))
    appartment = db.Column(db.String(120))
    city =       db.Column(db.Unicode)
    state =       db.Column(db.Unicode)
    loc_lat =       db.Column(db.Unicode)
    loc_lon =       db.Column(db.Unicode)
    zipcode  = db.Column(db.Integer, nullable=False)
    company =       db.Column(db.Unicode)
    date_created = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
     

    def __init__(self,users_id,address_line1,address_line2,appartment,city,state,zipcode,company):
        self.users_id = users_id
        self.address_line1 = address_line1
        self.address_line2 = address_line2
        self.appartment = appartment
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.company = company
        # self.loc_lat = loc_lat
        # self.loc_lon = loc_lon
        

    def __repr__(self):
        return '<UserAddress %r>' % self.name

  
# Speciality details, #It is the names of major specialities that we are using
class Speciality(db.Model):
    __tablename__ = 'speciality'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120))
    # date_created = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)
    doctor_speciality = db.relationship('DoctorSpeciality', backref='id_speciality', lazy=True)
    category = db.relationship('Category', backref='id_speciality', lazy=True)
    

    def __init__(self, name):
        self.name = name
    

    def __repr__(self):
        return '<Speciality %r>' % self.name

  

# Promo codes details 
class Doctor(db.Model):
    __tablename__ = 'doctor'

    id = db.Column(db.Integer, primary_key = True)
    first_name =       db.Column(db.Unicode)
    last_name =       db.Column(db.Unicode)
    # title = db.Column(db.String(80))
    email = db.Column(db.String(120))
    user_name = db.Column(db.String(120))
    # mci = db.Column(db.String(120))
    password = db.Column(db.String(120))
    phone = db.Column(db.String(120))
    fax = db.Column(db.String(120))
    image = db.Column(db.String(120))
    degree_title = db.Column(db.String(120))
    status =       db.Column(db.Integer,  nullable = True)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    # token = db.relationship('Token', backref='id_doctor', lazy=True)
    doctor_address = db.relationship('DoctorAddress', backref='id_doctor', lazy=True)
    doctor_speciality = db.relationship('DoctorSpeciality', backref='id_doctor', lazy=True)
    doctor_codes = db.relationship('DoctorCodes', backref='id_doctor', lazy=True)
    doctor_procedure = db.relationship('DoctorProcedure', backref='id_doctor', lazy=True)
        
     

    def __init__(self,first_name,last_name,email,user_name,password,phone,fax,image,degree_title,status):
        self.first_name = first_name
        self.last_name = last_name
        # self.title = title
        self.email = email
        self.user_name = user_name
        # self.mci = mci
        self.password =password
        self.phone = phone
        self.fax = fax
        self.image = image
        self.degree_title = degree_title
        self.status = status           

    def __repr__(self):
        return '<Doctor %r>' % self.first_name

  
# User address details 
class DoctorAddress(db.Model):
    __tablename__ = 'doctor_address'

    id = db.Column(db.Integer, primary_key = True)
    doctor_id = db.Column(db.Integer, db.ForeignKey('doctor.id'), nullable=False)
    
    address_line1 = db.Column(db.String(120))
    address_line2 = db.Column(db.String(120))
    appartment = db.Column(db.String(120))
    city =       db.Column(db.Unicode)
    state =       db.Column(db.Unicode)
    status  = db.Column(db.Integer, nullable=False)
    loc_lat =       db.Column(db.Unicode)
    loc_lon =       db.Column(db.Unicode)
    zipcode =       db.Column(db.Unicode)
    # doctor_procedure = db.relationship('DoctorProcedure', backref='id_doctor_address', lazy=True)

    def __init__(self,doctor_id,address_line1,address_line2,appartment,city,state,status ,loc_lat,loc_lon,zipcode):
        self.doctor_id = doctor_id 
        self.address_line1 = address_line1 
        self.address_line2 = address_line2
        self.appartment = appartment
        self.city = city       
        self.state = states
        self.status = status
        self.loc_lat =loc_lat
        self.loc_lon = loc_lon
        self.zipcode = zipcode
    
     

    def __repr__(self):
        return '<DoctorAddress %r>' % self.name
  
# Promo codes details 
class DoctorSpeciality(db.Model):
    __tablename__ = 'doctor_speciality'

    id = db.Column(db.Integer, primary_key = True)
    doctor_id = db.Column(db.Integer, db.ForeignKey('doctor.id'), nullable=False)
    speciality =    db.Column(db.Integer, db.ForeignKey('speciality.id'), nullable=False)
    speciality_name = db.Column(db.String(120))
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
        
     

    def __init__(self,doctor_id,speciality,speciality_name):
        self.doctor_id = doctor_id
        self.speciality = speciality
        self.speciality_name = speciality_name

    def __repr__(self):
        return '<DoctorSpeciality %r>' % self.speciality_name

  


# Promo codes details 
class DoctorCodes(db.Model):
    __tablename__ = 'doctor_codes'

    id = db.Column(db.Integer, primary_key = True)
    code = db.Column(db.String(120))
    doctor_id = db.Column(db.Integer, db.ForeignKey('doctor.id'),nullable=False)
    price = db.Column(db.String(120))
    status  = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
        
     

    def __init__(self,code,doctor_id,price,status):
        self.code = code
        self.doctor_id = doctor_id
        self.price = price
        self.status =status


    def __repr__(self):
        return '<DoctorCodes %r>' % self.name

  



# Body parts list 
class BodyPart(db.Model):
    __tablename__ = 'body_part'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120))
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    category = db.relationship('Category', backref='id_body_part', lazy=True)
                
     

    def __init__(self,name):
        self.name = name

    def __repr__(self):
        return '<BodyPart %r>' % self.name

  


#It is the combination of speciality and doctor
class Category(db.Model):
    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120))
    short_name = db.Column(db.String(120))
    speciality =  db.Column(db.Integer, db.ForeignKey('speciality.id'), nullable=False)
    body_part =    db.Column(db.Integer, db.ForeignKey('body_part.id'), nullable=False)
    image = db.Column(db.String(120))
    desc = db.Column(db.String(120))
    status  = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    procedure = db.relationship('Procedure', backref='id_category', lazy=True)
                
     

    def __init__(self,name,short_name,speciality,body_part,image,desc,status):
        self.name = name
        self.short_name = short_name
        self.speciality = speciality
        self.body_part = body_part
        self.image = image
        self.desc = desc
        self.status = status
        


    def __repr__(self):
        return '<Category %r>' % self.name

  


#It is the sublist of each category can be one to many from category
class Procedure(db.Model):
    __tablename__ = 'procedure'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120))
    short_name = db.Column(db.String(120))
    category =   db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)
    sku = db.Column(db.String(120))
    cpt = db.Column(db.String(120))
    desc = db.Column(db.String(120))
    status  = db.Column(db.Integer, nullable=False)
    price = db.Column(db.String(120))
    # "detail =        [{
    #         data = db.Column(db.String(120))
    #     }],
    # healora_percentage = db.Column(db.String(120))
    insurance_price = db.Column(db.String(120))
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    procedure_detail = db.relationship('ProcedureDetail', backref='id_procedure', lazy=True)
    doctor_procedure = db.relationship('DoctorProcedure', backref='id_procedure', lazy=True)   
     

    def __init__(self,name,short_name,sku,cpt,desc,status,price,healora_percentage,insurance_price):
        self.name = name
        self.short_name = short_name
        self.sku=sku
        self.cpt = cpt
        self.desc = desc
        self.status = status
        self.price = price
        self.healora_percentage = healora_percentage
        self.insurance_price = insurance_price

    def __repr__(self):
        return '<Procedure %r>' % self.name

class ProcedureDetail(db.Model):
    __tablename__ = 'procedure_detail'

    id = db.Column(db.Integer, primary_key = True)
    detail = db.Column(db.String(120))
    procedure_id =   db.Column(db.Integer, db.ForeignKey('procedure.id'), nullable=False)
    # status  = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


    def __init__(self,detail,procedure_id):
        self.detail = detail
        self.procedure_id = procedure_id
        
    def __repr__(self):
        return '<ProcedureDetail %r>' % self.detail
 



#It is the list of doctor prices of all procedures which they can change accordingly
class DoctorProcedure(db.Model):
    __tablename__ = 'doctor_procedure'

    id = db.Column(db.Integer, primary_key = True)
    procedure = db.Column(db.Integer, db.ForeignKey('procedure.id'),  nullable=False)
    doctor = db.Column(db.Integer, db.ForeignKey('doctor.id'),  nullable=False)
    status = db.Column(db.Integer, nullable=False)
    percentage = db.Column(db.String(120))
    total = db.Column(db.String(120))
    # doctor_address =   db.Column(db.Integer, db.ForeignKey('doctor_address.id'),  nullable=False)
    doctor_price = db.Column(db.String(120))
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    order = db.relationship('Order', backref='id_doctor_procedure', lazy=True)
     

    def __init__(self,procedures,doctor,status,percentage,total,doctor_price):
        self.procedures = procedure
        self.doctor = doctor
        self.status = status
        self.percentage = percentage
        self.total = total
        # self.doctor_address = doctor_address
        self.doctor_price = doctor_price
        
    def __repr__(self):
        return '<DoctorProcedure %r>' % self.status




#It is the list of doctor prices of all procedures which they can change accordingly
class Order(db.Model):
    __tablename__ = 'order'

    id = db.Column(db.Integer, primary_key = True)
    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    doctor_procedure =   db.Column(db.Integer, db.ForeignKey('doctor_procedure.id'), nullable=False)
    order_id = db.Column(db.String(120))
    temp = db.Column(db.String(120))
    payment_method = db.Column(db.String(120))
    payment_id = db.Column(db.String(120))
    token = db.Column(db.String(120))
    payer_id = db.Column(db.String(120))
    status = db.Column(db.String(120))
    request = db.Column(db.String(120))
    responce = db.Column(db.String(120))
    responce_execute = db.Column(db.String(120))
    total_amount = db.Column(db.String(120))
    balance = db.Column(db.String(120))
    reserve_price = db.Column(db.String(120))
    comments = db.Column(db.String(120))
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    payment_time = db.Column(db.String(120))
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
     

    def __init__(self,users_id,doctor_procedure,order_id,temp,payment_method,payment_id,token,payer_id,status,request,total_amount,balance,reserve_price,comments):
        self.user = users_id
        self.doctor_procedure =doctor_procedure
        self.order_id = order_id
        self.temp = temp
        self.payment_method = payment_method
        self.payment_id = payment_id
        self.token = token
        self.payer_id = payer_id
        self.status = status
        # self.responce_execute = responce_execute
        self.total_amount = total_amount
        self.balance = balance
        self.reserve_price = reserve_price
        self.comments = comments
        # self.payment_time =payment_time
        self.request = request
        # self.responce = responce
        

    def __repr__(self):
        return '<Order %r>' % self.status


#It is the list of doctor prices of all procedures which they can change accordingly
class Blog(db.Model):
    __tablename__ = 'blog'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120))
    podcast= db.Column(db.String(120))
    short_name = db.Column(db.String(120))
    short_text = db.Column(db.String(120))
    url = db.Column(db.String(120))
    category = db.Column(db.String(120))
    tags = db.Column(db.String(120))
    image = db.Column(db.String(120))
    image_detail = db.Column(db.String(120))
    user_type = db.Column(db.String(120))
    created_by = db.Column(db.String(120))
    desc = db.Column(db.String(120))
    status  = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
 

    def __init__(self,name,podcast,short_name,short_text,url,category,tags,image,image_detail,user_type,created_by,desc,status):
        self.name = name
        self.podcast = podcast
        self.short_name = short_name
        self.short_text =short_text
        self.url = url
        self.category = category
        self.tags = tags
        self.image = image
        self.image_detail =image_detail
        self.user_type = user_type
        self.created_by = created_by
        self.desc = desc
        self.status = status

    def __repr__(self):
        return '<Blog %r>' % self.status


#It is the list of doctor prices of all procedures which they can change accordingly
class Comment(db.Model):
    __tablename__ = 'comment'

    id = db.Column(db.Integer, primary_key = True)
    blog_url = db.Column(db.String(120))
    desc = db.Column(db.String(120))
    name = db.Column(db.String(120))
    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    status  = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    

    def __init__(self,blog_url,desc,name,users_id,status):
        self.blog_url = blog_url
        self.desc = desc
        self.name = name
        self.user = users_id
        self.status =status

    def __repr__(self):
        return '<Comment %r>' % self.status






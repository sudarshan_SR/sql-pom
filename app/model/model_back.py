__author__ = 'Kunal Monga'

# import requirement
# We are creating all tables in single file.
# In this we have structure and validators 

from mongokit import Document
from app import app, db
from mongokit import ValidationError
from datetime import datetime

# Classes starts here
# Main User table
class Users(Document):
    __collection__ = 'users'

    structure = {
        "first_name" : unicode,
        "last_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.user_name)

db.register([Users])

# Classes starts here
# Main User table
class Token(Document):
    __collection__ = 'token'

    structure = {
        "user_id" : Users,
        "token" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Token %r>' % (self.token)

db.register([Token])

# User address details 
class UserAddress(Document):
    __collection__ = 'user_address'

    structure = {
        "user_id" : Users,
        "address_line1" : str,
        "address_line2" : str,
        "appartment" : str,
        "city" : unicode,
        "state" : unicode,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "zipcode" : int,
        "company" : unicode,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<UserAddress %r>' % (self.name)

db.register([UserAddress])

# Speciality details, It is the names of major specialities that we are using
class Speciality(Document):
    __collection__ = 'speciality'

    structure = {
        "name" : str,
        "date_created" : datetime
                
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Speciality %r>' % (self.name)

db.register([Speciality])


# Promo codes details 
class Doctor(Document):
    __collection__ = 'doctor'

    structure = {
        "first_name" : unicode,
        "last_name" : unicode,
        "title" : str,
        "email" : str,
        "user_name" : str,
        "mci" : str,
        "password" : str,
        "phone" : str,
        "fax" : str,
        "image" : str,
        "degree_title" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Doctor %r>' % (self.first_name)

db.register([Doctor])

# User address details 
class DoctorAddress(Document):
    __collection__ = 'doctor_address'

    structure = {
        "doctor_id" : Doctor,
        "address_line1" : str,
        "address_line2" : str,
        "appartment" : str,
        "city" : unicode,
        "state" : unicode,
        "status" : int,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "zipcode" : unicode
    }
    use_dot_notation = True

    def __repr__(self):
        return '<DoctorAddress %r>' % (self.name)
db.register([DoctorAddress])

# Promo codes details 
class DoctorSpeciality(Document):
    __collection__ = 'doctor_speciality'

    structure = {
        "doctor_id" : Doctor,
        "speciality" : Speciality,
        "speciality_name" : str,
        "date_created" : datetime,
        "date_updated" : datetime
        
    }
    use_dot_notation = True

    def __repr__(self):
        return '<DoctorSpeciality %r>' % (self.speciality_name)

db.register([DoctorSpeciality])



# Promo codes details 
class DoctorCodes(Document):
    __collection__ = 'doctor_codes'

    structure = {
        "code" : str,
        "doctor_id" : Doctor,
        "price" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
        
    }
    use_dot_notation = True

    def __repr__(self):
        return '<DoctorCodes %r>' % (self.name)

db.register([DoctorCodes])




# Body parts list 
class BodyPart(Document):
    __collection__ = 'body_part'

    structure = {
        "name" : str,
        "date_created" : datetime
                
    }
    use_dot_notation = True

    def __repr__(self):
        return '<BodyPart %r>' % (self.name)

db.register([BodyPart])


# It is the combination of speciality and doctor
class Category(Document):
    __collection__ = 'category'

    structure = {
        "name" : str,
        "short_name" : str,
        "speciality" : Speciality,
        "body_part" : BodyPart,
        "image" : str,
        "desc" : str,
        "status" : int,
        "date_created" : datetime
                
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Category %r>' % (self.name)

db.register([Category])


# It is the sublist of each category can be one to many from category
class Procedure(Document):
    __collection__ = 'procedure'

    structure = {
        "name" : str,
        "short_name" : str,
        "category" : Category,
        "sku" : str,
        "cpt" : str,
        "desc" : str,
        "status" : int,
        "price" : str,
        "detail" :  [{
                "data" : str
            }],
        "healora_percentage" : str,
        "insurance_price" : str,
        "date_created" : datetime
                
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Procedure %r>' % (self.name)

db.register([Procedure])



# It is the list of doctor prices of all procedures which they can change accordingly
class DoctorProcedure(Document):
    __collection__ = 'doctor_procedure'

    structure = {
        "procedure" : Procedure,
        "doctor" : Doctor,
        "status" : int,
        "percentage" : str,
        "total" : str,
        "doctor_address" : DoctorAddress,
        "doctor_price" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<DoctorProcedure %r>' % (self.status)

db.register([DoctorProcedure])



# It is the list of doctor prices of all procedures which they can change accordingly
class Order(Document):
    __collection__ = 'order'

    structure = {
        "user" : Users,
        "doctor_procedure" : DoctorProcedure,
        "order_id" : str,
        "temp" : str,
        "payment_method" : str,
        "payment_id" : str,
        "token" : str,
        "payer_id" : str,
        "status" : str,
        "request" : str,
        "responce" : str,
        "responce_execute" : str,
        "total_amount" : str,
        "balance" : str,
        "reserve_price" : str,
        "comments" : str,
        "date_created" : datetime,
        "payment_time" : str,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Order %r>' % (self.status)

db.register([Order])

# It is the list of doctor prices of all procedures which they can change accordingly
class Blog(Document):
    __collection__ = 'blog'

    structure = {
        "name" : str,
        "short_name" : str,
        "short_text" : str,
        "url" : str,
        "category" : str,
        "tags" : str,
        "image" : str,
        "image_detail" : str,
        "user_type" : str,
        "created_by" : str,
        "desc" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Blog %r>' % (self.status)

db.register([Blog])


# It is the list of doctor prices of all procedures which they can change accordingly
class Comment(Document):
    __collection__ = 'comment'

    structure = {
        "blog_url" : str,
        "desc" : str,
        "name" : str,
        "user" : Users,
        "status" : int,
        "date_created" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Comment %r>' % (self.status)

db.register([Comment])





